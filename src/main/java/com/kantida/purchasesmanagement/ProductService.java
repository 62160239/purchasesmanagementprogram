/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.purchasesmanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ProductService {
    private static ArrayList<Product> ProductList = null;
    
    static{
        ProductList = new ArrayList<>();
        //Mock data 
        ProductList.add(new Product("1","Rice","easy",15.0,1));
        ProductList.add(new Product("2","Sausage","Ceepee",35.0,1));
        ProductList.add(new Product("3","Snack","ArhanYodkhun",10.5,2));
    }
    
    public static boolean addProduct(Product product){
        ProductList.add(product);
        save();
        return true;
    }
    
    public static boolean delProduct(Product product){
        ProductList.remove(product);
        save();
        return true;
    }
    
    public static boolean delProduct(int index){
        ProductList.remove(index);
        save();
        return true;
    }
    //Read
    public static ArrayList<Product> getProduct(){
        return ProductList;
    }
    //Update(U)
    public static boolean updateProduct(int index, Product product){
        ProductList.set(index, product);
        save();
        return true;
    }
    
    public static Product getProduct(int index) {
        return ProductList.get(index);
    }

    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        
        try {
            file = new File("mamay.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProductList);
            oos.close();
            fos.close(); 
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        file = new File("mamay.dat");
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProductList = (ArrayList<Product>)ois.readObject();
            ois.close();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }   catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}
}


